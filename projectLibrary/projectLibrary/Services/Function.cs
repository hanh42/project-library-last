﻿using System;
using System.Collections.Generic;
using projectLibrary.Entity;

namespace projectLibrary.Services
{
    static class Function
    {
        //// <summary>
        /// lấy dữ liệu user từ screen và so sánh  với dữ liệu trong file de thuc hien login.
        /// </summary>
        /// <param name="username">tên account do người dùng nhập vào.</param>
        /// <param name="passworld">mật khẩu do người dùng nhập vào.</param>
        /// <returns>kết quả việc đăng nhập</returns>
        internal static bool Login(string username = "", string password = "")
        {
            bool result = false;
<<<<<<< HEAD
            var x = User.UsersList.Find(x => x.Name == username);
            if (x.Password == password && x.Status == true)
=======
            var x = User.UserList.Find(x => x.Name == username);
            if (x.Password == passworld && x.Status==true)
>>>>>>> develop
            {
                return true;
            }
            return result;
        }

        /// <summary>
        /// khóa tài khoản nếu nhập sai đúng 3 lần
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
<<<<<<< HEAD
        internal static bool LockLogin(string userName)
        {
            bool result = false;
            var user = User.UsersList.Find(x => x.Name == userName);
=======
        internal static bool LockLogin(int id)
        {
            bool result = false;
            var user = User.UserList.Find(x => x.Id == id);
>>>>>>> develop
            if (user != null && user.Status)
            {
                user.Status = false;
                result = true;
            }
            return result;
        }

        //****quan ly sach****

        /// <summary>
        /// thực hiện lấy thông tin của tất cả các cuốn sách.
        /// </summary>
        /// <returns>list sách.</returns>
        internal static List<Book> booksInformation()
        {
            List<Book> booksList = Book.BooksList;
            return booksList;
        }

        /// <summary>
        /// thêm sách vào thư viện.
        /// </summary>
        /// <param name="name">tên sách.</param>
        /// <param name="price">giá.</param>
        /// <param name="releaseYear">năm phát hành.</param>
        /// <param name="numPage">số trang.</param>
        /// <param name="inputDate">ngày nhập kho.</param>
        /// <param name="publisherId">mã nhà xuất bản.</param>
        /// <param name="authorId">mã tác giả.</param>
        /// <returns>kết quả việc tạo sách.</returns>
        internal static Book BookAdd(string name = "", int price = 0, int releaseYear = 0, int numPage = 0, string inputDate = "", int publisherId = 0, int authorId = 0)
        {
<<<<<<< HEAD
            Book book = new Book(name: name, price: price, releaseYear: releaseYear, numPage: numPage, inputDate: inputDate, publisherId: publisherId, authorId: authorId);
=======
            Book book = new Book(name: name,price: price,releaseYear: releaseYear, numPage: numPage, inputDate: inputDate, publisherId: publisherId, authorId: authorId);
>>>>>>> develop
            return book;
        }

        /// <summary>
        /// xóa sách.
        /// </summary>
        /// <param name="BookId">mã sách cần xóa.</param>
        /// <returns>kết quả việc xóa sách.</returns>
        internal static bool BookDelete(int BookId = 0)
        {
            bool result = true;
            var book = Book.BooksList.Find(x => x.Id == BookId);
<<<<<<< HEAD
            if (book == null || book.Status != 0)
            {
                result = false;
            }
            else
            {
                book.AuthorOfBook.BooksList.Remove(book);
                book.PublisherOfBook.PublisherBooksList.Remove(book);
                Book.BooksList.Remove(book);
            }
=======
                if (book == null || book.Status != 0 )
                {
                     result = false;
                }
                else
                {
                    book.AuthorOfBook.BooksList.Remove(book);
                    book.PublisherOfBook.PublisherBooksList.Remove(book);
                    Book.BooksList.Remove(book);
                }
>>>>>>> develop
            return result;
        }

        //*****qly phieu muon******

        /// <summary>
        /// hiển thị thông tin của tất cả các phiếu mượn.
        /// </summary>
        /// /// <returns>list phiếu mượn.</returns>
        internal static List<Card> CardsInformation()
        {
            List<Card> cardList = Card.CardList;
            return cardList;
        }

        /// <summary>
        ///Mượn sách.
        /// </summary>
        /// <param name="bookId">mã sách người dùng nhập.</param>
        /// <param name="ReaderId">mã bạn đọc người dùng nhập.</param>
        /// <returns>kết quả việc thêm sách.</returns>
        internal static Card CardAdd(int bookId = 0, int ReaderId = 0)
        {
<<<<<<< HEAD
            Card card = new Card(bookId, ReaderId);
=======
            Card card = new Card(bookId,ReaderId);
>>>>>>> develop
            return card;
        }

        /// <summary>
        /// trả sách.
        /// </summary>
        /// <param name="cardId">mã card bạn muốn trả.</param>
        /// <returns>kết quả của việc trả sách.</returns>
        internal static bool CardReturn(int cardId = 0)
        {
            bool result = false;
            var card = Card.CardList.Find(x => x.Id == cardId);
            if (card != null && card.Status != 0)
            {
<<<<<<< HEAD
                card.BookCard.Status = 0;
                card.Status = 0;
                result = true;
=======
                    card.BookCard.Status = 0;
                    card.Status = 0;
                    result =  true;
>>>>>>> develop
            }
            return result;
        }

    }
}
